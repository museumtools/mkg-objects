# Script to get MKG collection via SPARQL
https://museumtools.artserver.org/wiki/Help:MKG

## before running:
get secretes: `git clone git@gitlab.com:museum` (close repository)

## run: 
`python -m MKG_objects`