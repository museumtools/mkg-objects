import requests, os, json
from typing import Dict
from pathlib import Path
from SPARQLWrapper import SPARQLWrapper, JSON
from MKG_objects.globals import useragent, europeana_api_key
from urllib.parse import urlparse, quote_plus
from rdflib import Graph

current_dir = Path(__file__).parent.absolute()


class Artifact:
    def __init__(self, sparql_item_printout):
        # self.sparql_item_printout = sparql_item_printout
        self.uri = sparql_item_printout['uri']['value']
        self.imgurl = sparql_item_printout['image']['value']
        self.europeanaIDurl = sparql_item_printout['europeanaIDurl']['value']
        self.record = None
        self.record_rdf = None

    def download(self):
        img_request = requests.get(url=self.imgurl)
        if 'image' in img_request.headers['content-type']:
            f_ext = img_request.headers['content-type'].replace('image/', '')
            f_name = (urlparse(img_request.url).path).split('/')[-1]
            f_name = f"{f_name}.{f_ext}"
            f_path = current_dir / 'imgs'/ f_name
            if not os.path.isfile(f_path):
                with open(f_path, 'wb') as img_file:
                    img_file.write(img_request._content)
                    print(f'Saved {f_path}')

    def get_europeana_record_rdf(self):
        url_path_list = (urlparse(self.europeanaIDurl).path).split('/')
        record_id = f'{url_path_list[-2]}/{url_path_list[-1]}'
        record_api_base = 'https://www.europeana.eu/api/v2/record/'
        record_api = f'{record_api_base}{record_id}.rdf?profile=full&wskey=' \
                     f'{europeana_api_key}'
        graph = Graph()
        graph.parse(record_api)
        record_request = requests.get(url=record_api)
        self.record_rdf = record_request._content
        # import pdb; pdb.set_trace()
        with open(current_dir / 'record.rq', 'r') as sparl_f:
            sparql_record_query = sparl_f.read()
        for row in graph.query(sparql_record_query):
            print(f'ROW: {row}')
        # self.record = json.loads(record_request._content)


def query() -> Dict:
    sparql_endpoint = 'http://sparql.europeana.eu/'
    endpoint = SPARQLWrapper(endpoint=sparql_endpoint,
                             agent=useragent)
    with open(current_dir / 'MGK_objects.rq', 'r') as sparl_f:
        sparql_query = sparl_f.read()
        # print(sparql_query)
    endpoint.setQuery(sparql_query)
    endpoint.setReturnFormat(JSON)
    results = endpoint.query().convert()
    for result in results['results']['bindings']:
        artifact = Artifact(sparql_item_printout=result)
        yield artifact


def collectobjects():
    for artifact_n, artifact in enumerate(query(),start=1):
        print(artifact)
        # artifact.download()

        artifact.get_europeana_record_rdf()

    print(f'TOTAL artifactS RETURNED: {artifact_n}')


if __name__ == '__main__':
    printout1item = {
        'uri': {
        'type': 'uri',
        'value': 'http://data.europeana.eu/aggregation/provider/2048429/item_22HQUSRDAS7AM4I4BX7SOHDRJGQ3ENTE'},
        'image': {
            'type': 'uri',
            'value': 'http://resources.digicult-museen.net/dam/provided_image/m57d2fd8dc5493'},
        'europeanaIDurl': {
            'type': 'uri',
            'value': 'http://data.europeana.eu/item/2048429/item_22HQUSRDAS7AM4I4BX7SOHDRJGQ3ENTE'}
    }
    artifact = Artifact(sparql_item_printout=printout1item)
    artifact.get_europeana_record_rdf()
    # artifact_info = artifact.record['object']