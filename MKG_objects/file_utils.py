import yaml
import sys
from pathlib import Path
from typing import Dict


def relative_read_f(relativepath2f: str) -> str:
    path_file = Path(__file__).parent / relativepath2f
    with open(path_file, 'r') as f:
        f_content = f.read()
    return f_content


def yaml2dict(path: str) -> Dict:
    with open(path, 'r') as yaml_f:
        yaml_content = yaml_f.read()
        yaml_dict = yaml.safe_load(yaml_content)
    return yaml_dict


def dict2yaml(path: str, data: Dict):
    with open(path, 'w') as yaml_f:
        yaml.safe_dump(data=data, stream=yaml_f)


def yaml_get_source(relativepath2f: str, method='read', data=None) -> Dict:
    # print('CWD:', Path.cwd())
    # print('parent:', Path(__file__).parent)
    path_file = Path(__file__).parent / relativepath2f
    if method == 'read':
        yamldict = yaml2dict(path_file)
        return yamldict
    elif method == 'write':
        dict2yaml(path=path_file, data=data)


def get_secret(key: str) -> str:
    secrets_dict = yaml_get_source(relativepath2f='../secrets/secrets.yml')
    return secrets_dict.get(key)